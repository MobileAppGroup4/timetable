package com.example.timetogo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.timetogo.dto.ConnectionDto;

import java.text.SimpleDateFormat;
import java.util.List;

public class ConnectionAdapter extends BaseAdapter {

    private Context context;
    private List<ConnectionDto> connections;
    private SimpleDateFormat df = new SimpleDateFormat("HH:mm E dd-MM-yyyy");

    public ConnectionAdapter(Context context, List<ConnectionDto> connections) {
        this.context = context;
        this.connections = connections;
    }

    @Override
    public int getCount() {
        if (connections == null) {
            return 0;
        }
        return connections.size();
    }

    @Override
    public Object getItem(int position) {
        return connections.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ++position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(getLayoutIdForViewType(position), parent, false);
        }
        ConnectionDto actualConnection = (ConnectionDto) getItem(position);

        TextView idTextView = (TextView) convertView.findViewById(R.id.id);
        idTextView.setText(getItemId(position) + "");

        TextView nameTextView = (TextView) convertView.findViewById(R.id.departure);
        nameTextView.setText(context.getString(R.string.textDeparture) + " " + actualConnection.from.station.name + " " + df.format(actualConnection.from.departure));

        TextView addressTextView = (TextView) convertView.findViewById(R.id.arrival);
        addressTextView.setText(context.getString(R.string.textArrival) + " " + actualConnection.to.station.name + " " + df.format(actualConnection.to.arrival));

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return position % 2;
    }

    private int getLayoutIdForViewType(int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case 0:
                return R.layout.listitem_id_left;
        }

        return R.layout.listitem_id_right;
    }

}
