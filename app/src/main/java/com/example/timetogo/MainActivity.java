package com.example.timetogo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.timetogo.dto.ConnectionContainerDto;
import com.example.timetogo.dto.StationboardContainerDto;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.departurePlace)
    public AutoCompleteTextView departurePlace;

    @BindView(R.id.arrivalPlace)
    public AutoCompleteTextView arrivalPlace;

    @BindView(R.id.startTime)
    public TextView startTime;

    private ListView listView;
    private Context context;
    private ConnectionAdapter adapter;
    private boolean isArrivalTime;
    private String formattedDate;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ButterKnife.bind(this);

        listView = (ListView) findViewById(R.id.listView);
        context = this;

        Date date = Calendar.getInstance().getTime();
        formattedDate = df.format(date);
        startTime.setText(formatDate(date));

        SharedPreferences sharedPref = androidx.preference.PreferenceManager
                .getDefaultSharedPreferences(this);
        arrivalPlace.setText(sharedPref.getString("homeAddress", getString(R.string.homeAddress_default)));

        FloatingActionButton fab = findViewById(R.id.floatingActionButtonTakeMeHome);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arrivalPlace.setText(sharedPref.getString("homeAddress", getString(R.string.homeAddress_default)));
                setConnectionViewInvisible();
            }
        });

    }

    @OnCheckedChanged(R.id.checkBoxIsArrivalTime)
    public void onCheckBoxChange(boolean value) {
        setConnectionViewInvisible();
        isArrivalTime = value;
    }

    @OnClick(R.id.switchDestinations)
    public void onChangeClick(View v) {
        setConnectionViewInvisible();
        String buffer = departurePlace.getText().toString();
        departurePlace.setText(arrivalPlace.getText().toString());
        arrivalPlace.setText(buffer);
    }

    @OnClick(R.id.showMoreConnections)
    public void onShowMoreConnectionClick() {
        searchConnections("16");
    }

    @OnClick(R.id.search)
    public void onSearchClick() {
        searchConnections("4");
    }

    private void searchConnections(String limit) {
        final ProgressBar simpleProgressBar = findViewById(R.id.simpleProgressBar);
        setConnectionViewInvisible();
        simpleProgressBar.setVisibility(View.VISIBLE);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://transport.opendata.ch/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ConnectionService service = retrofit.create(ConnectionService.class);

        service.searchConnections(departurePlace.getText().toString(), arrivalPlace.getText().toString(), formattedDate, isArrivalTime ? 1 : 0, limit).enqueue(new Callback<ConnectionContainerDto>() {

            @Override
            public void onResponse(Call<ConnectionContainerDto> call, Response<ConnectionContainerDto> response) {
                if (response.isSuccessful()) {
                    ConnectionContainerDto connectionsContainer = response.body();
                    // Handle result...
                    connectionsContainer.updateStaticList();
                    adapter = new ConnectionAdapter(context, ConnectionContainerDto.getAllConnections());
                    listView.setAdapter(adapter);

                    if (connectionsContainer.connections.size() <= 4) {
                        findViewById(R.id.showMoreConnections).setVisibility(View.VISIBLE);
                    }
                    listView.setVisibility(View.VISIBLE);
                    simpleProgressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ConnectionContainerDto> call, Throwable t) {
                simpleProgressBar.setVisibility(View.INVISIBLE);
                Toast toast = Toast.makeText(context, getString(R.string.connectionProblem), Toast.LENGTH_LONG);
                toast.show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(MainActivity.this, SettingsActivity.class);
            MainActivity.this.startActivity(settingsIntent);
        }

        if (id == R.id.action_about) {
            Intent aboutIntent = new Intent(MainActivity.this, AboutActivity.class);
            MainActivity.this.startActivity(aboutIntent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendMessage(View view) {
        new SingleDateAndTimePickerDialog.Builder(this)
                .title(getString(R.string.dateTimePickerTitle))
                .curved()
                .displayMinutes(true)
                .displayHours(true)
                .displayDays(true)
                .displayMonth(true)
                .displayYears(true)
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {
                        formattedDate = df.format(date);
                        startTime.setText(formatDate(date));
                        setConnectionViewInvisible();
                    }
                }).display();
    }

    private String formatDate(Date date) {
        return new SimpleDateFormat("E dd-MM-yyyy HH:mm").format(date);
    }

    private void setConnectionViewInvisible() {
        listView.setVisibility(View.INVISIBLE);
        findViewById(R.id.showMoreConnections).setVisibility(View.INVISIBLE);
    }


    @OnTextChanged(R.id.departurePlace)
    public void onDepartureTextChanged() {
        setLocationNames(findViewById(R.id.departurePlace));
    }

    @OnTextChanged(R.id.arrivalPlace)
    public void onArrivalTextChanged() {
        setLocationNames(findViewById(R.id.arrivalPlace));
    }

    private void setLocationNames(AutoCompleteTextView actualTextView) {
        ConnectionService service = createConnectionService();
        service.searchLocations(actualTextView.getText().toString()).enqueue(new Callback<StationboardContainerDto>() {

            @Override
            public void onResponse(Call<StationboardContainerDto> call, Response<StationboardContainerDto> response) {
                if (response.isSuccessful()) {
                    StationboardContainerDto stationboardContainer = response.body();
                    // Handle result...
                    stationboardContainer.updateLocationNames();
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_dropdown_item_1line, StationboardContainerDto.locationNames);
                    actualTextView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<StationboardContainerDto> call, Throwable t) {
                Toast toast = Toast.makeText(context, getString(R.string.connectionProblem), Toast.LENGTH_LONG);
                toast.show();
            }

        });
    }

    private ConnectionService createConnectionService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://transport.opendata.ch/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(ConnectionService.class);
    }

}



