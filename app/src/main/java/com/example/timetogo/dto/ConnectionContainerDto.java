package com.example.timetogo.dto;

import java.util.List;

public class ConnectionContainerDto {
    
    public List<ConnectionDto> connections;
    public static List<ConnectionDto> connections2;

    public void updateStaticList() {
        connections2 = this.connections;
    }

    public static List<ConnectionDto> getAllConnections() {
        return connections2;
    }

}
