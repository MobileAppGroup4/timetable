package com.example.timetogo.dto;

import java.util.List;

public class StationboardContainerDto {


    public List<StationboardDto> stations;
    public static String[] locationNames;

    public void updateLocationNames(){
        locationNames = new String[stations.size()];
        for (int i=0; i<stations.size();i++) {
            locationNames[i] =this.stations.get(i).name;
        }
    }

}
