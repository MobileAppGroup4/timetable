package com.example.timetogo.dto;


import java.util.Date;

public class ArrivalLocationDto extends LocationDto  {

    public Date arrival;
    public int platform;
    public String name;
    public StationDto station;

}
