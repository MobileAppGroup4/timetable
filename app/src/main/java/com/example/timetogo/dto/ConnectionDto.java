package com.example.timetogo.dto;

public class ConnectionDto {

    public DepartureLocationDto from;
    public ArrivalLocationDto to;
}
