package com.example.timetogo.dto;


import java.util.Date;

public class DepartureLocationDto extends LocationDto {

    public Date departure;
    public int platform;
    public String name;
    public StationDto station;

}
