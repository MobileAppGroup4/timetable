package com.example.timetogo;


import com.example.timetogo.dto.ConnectionContainerDto;
import com.example.timetogo.dto.StationboardContainerDto;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;



public interface ConnectionService {

    @GET("connections")
    Call<ConnectionContainerDto> searchConnections(@Query("from") String origin, @Query("to") String destination,@Query("datetime") String dateTime,@Query("isArrivalTime") int isArrivalTime, @Query("limit") String limit);

    @GET("locations")
    Call<StationboardContainerDto> searchLocations(@Query("query") String stationName);

}
